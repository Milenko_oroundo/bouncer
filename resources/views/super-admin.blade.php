@extends('layouts.app')
@section('content')

    @can("*")
        <label for="name">Turn off/on permissions and roles</label>
        <input type="checkbox" id="check" onclick="checking()" name="check">
        <script>
            function checking() {
                var checked = document.getElementById("check").checked;
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
//                        console.log(this.responseText);
                    }
                };
                xhttp.open("GET", "/check-ability?value=" + checked, true);
                xhttp.send();
            }
            $(document).ready(function(){
                var checked = document.getElementById("check").checked;
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        if(this.responseText == 1)
                            document.getElementById("check").checked = true;
                        else
                            document.getElementById("check").checked = false;
                    }
                };
                xhttp.open("GET", "/checked?value=" + checked, true);
                xhttp.send();
            });
        </script>
    @endcan
    </br></br></br>
    <button onclick="loadDoc()">Show</button>
        <p hidden>You are authorized to see this!!!</p>
        <script>
        function loadDoc() {
            @can('something123')
            $(document).ready(function () {
                $("p").show();
            });
            @endcan
            @cannot('something123')
            var xhttp1 = new XMLHttpRequest();
            xhttp1.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    if(this.responseText == 1)
                        alert("you are not authorized to see this!");
                    else{
                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                console.log(this.responseText);
                                $(document).ready(function(){
                                    $("p").show();
                                });
                            }
                        };
                        xhttp.open("GET", "/insert-ability?ability=something123", true);
                        xhttp.send();
                    }
                }
            };
            xhttp1.open("GET", "/checked?value=", true);
            xhttp1.send();
            @endcannot

        }
    </script>
@endsection
