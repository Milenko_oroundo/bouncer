<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Bouncer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\Database\Ability;
use DB;

class Controller extends BaseController
{
    public function __construct()
    {
        return $this->middleware('web');
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function test(Request $request)
    {
        $users = User::all();
        $user = User::all()->where('active' , 1)->first();
        if($user->email == 'mikomali93@gmail.com') {
                Bouncer::assign('super-admin')->to($user);
                Bouncer::allow('super-admin')->everything();
        }
        elseif($user->email == 'rasholetic@gmail.com') {
            Bouncer::assign('admin')->to($user);
            Bouncer::allow('admin')->to('something');
        }
        return view('super-admin')->with('users' , $users);
    }

    public function insertAbility(Request $request)
    {
        $user = User::all()->where('active' , 1)->first();
        Ability::create(['name' => $request->get('ability')]);
        Bouncer::assign('user')->to($user);
        Bouncer::allow('user')->to(Ability::all()->last());
        return response()->json([$user->name => $user->getAbilities()->last()->name]);

    }

    public function checkAbility(Request $request)
    {
        if($request->get('value') == 'true') {
            DB::table('users')->where(['email' => 'mikomali93@gmail.com'])->update(['checked' => true]);
            return response("on");
        }else
            DB::table('users')->where(['email' => 'mikomali93@gmail.com'])->update(['checked' => false]);
        return response("off");


    }

    public function checked(Request $request)
    {
        $user = User::all()->where('email' , 'mikomali93@gmail.com')->pluck('checked')->first();
        return $user;
//        return response()->json(['k' => $user]);
    }

}
